uses swagger http://swagger.io
uses swagger for springmvc https://github.com/martypitt/swagger-springmvc

to run spring boot app

mvn clean spring-boot:run

then goto

http://localhost:8080/docs/index.html

This should display the swagger information produced at http://localhost:8080/api-docs in a nice web page

The 4 restful URLS which should be picked up by swagger are:

http://localhost:8080/rest/users
http://localhost:8080/rest/users/{userId}
http://localhost:8080/rest/users/{userId}/tasks/
http://localhost:8080/rest/users/{userId}/tasks/{taskId}
