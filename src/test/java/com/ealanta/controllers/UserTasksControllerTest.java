package com.ealanta.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.hamcrest.Matchers.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.ealanta.UserTasksSwaggerApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = UserTasksSwaggerApplication.class)
@WebAppConfiguration
public class UserTasksControllerTest {

	@Autowired
	private WebApplicationContext wac;
	
	private MockMvc mockMvc;
	
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void testUsers() throws Exception {
		mockMvc.perform(get("/api/users"))
		.andExpect(jsonPath("$", hasSize(2)))
		.andExpect(jsonPath("$[0].id", is(1)))
		.andExpect(jsonPath("$[0].username", is("joebloggs")))
		.andExpect(jsonPath("$[0].firstname", is("Joe")))
		.andExpect(jsonPath("$[0].lastname", is("Bloggs")))
		.andExpect(jsonPath("$[0].tasks", hasSize(3)))
		
		.andExpect(jsonPath("$[0].tasks[0].id", is(11)))
		.andExpect(jsonPath("$[0].tasks[0].description", is("task eleven")))
		.andExpect(jsonPath("$[0].tasks[0].completed", is(false)))
		.andExpect(jsonPath("$[0].tasks[0].dueDate", is("2016-01-01T01:02:03Z")))
		.andExpect(jsonPath("$[0].tasks[0].priority", is("LO")))
		
		.andExpect(jsonPath("$[0].tasks[1].id", is(12)))
		.andExpect(jsonPath("$[0].tasks[1].description", is("task twelve")))
		.andExpect(jsonPath("$[0].tasks[1].completed", is(true)))
		.andExpect(jsonPath("$[0].tasks[1].dueDate", is("2016-01-02T02:03:04Z")))
		.andExpect(jsonPath("$[0].tasks[1].priority", is("MED")))

		.andExpect(jsonPath("$[0].tasks[2].id", is(13)))
		.andExpect(jsonPath("$[0].tasks[2].description", is("task thirteen")))
		.andExpect(jsonPath("$[0].tasks[2].completed", is(false)))
		.andExpect(jsonPath("$[0].tasks[2].dueDate", is("2016-01-03T03:04:05Z")))
		.andExpect(jsonPath("$[0].tasks[2].priority", is("HI")))

		.andExpect(jsonPath("$[1].id", is(2)))
		.andExpect(jsonPath("$[1].username", is("molliem")))
		.andExpect(jsonPath("$[1].firstname", is("Mollie")))
		.andExpect(jsonPath("$[1].lastname", is("Malone")))
		.andExpect(jsonPath("$[1].tasks", hasSize(3)))
		
		
		.andExpect(jsonPath("$[1].tasks[0].id", is(21)))
		.andExpect(jsonPath("$[1].tasks[0].description", is("task twenty one")))
		.andExpect(jsonPath("$[1].tasks[0].completed", is(false)))
		.andExpect(jsonPath("$[1].tasks[0].dueDate", is("2016-01-04T04:05:06Z")))
		.andExpect(jsonPath("$[1].tasks[0].priority", is("LO")))
		
		.andExpect(jsonPath("$[1].tasks[1].id", is(22)))
		.andExpect(jsonPath("$[1].tasks[1].description", is("task twenty two")))
		.andExpect(jsonPath("$[1].tasks[1].completed", is(true)))
		.andExpect(jsonPath("$[1].tasks[1].dueDate", is("2016-01-05T05:06:07Z")))
		.andExpect(jsonPath("$[1].tasks[1].priority", is("MED")))

		.andExpect(jsonPath("$[1].tasks[2].id", is(23)))
		.andExpect(jsonPath("$[1].tasks[2].description", is("task twenty three")))
		.andExpect(jsonPath("$[1].tasks[2].completed", is(false)))
		.andExpect(jsonPath("$[1].tasks[2].dueDate", is("2016-01-06T06:07:08Z")))
		.andExpect(jsonPath("$[1].tasks[2].priority", is("HI")))

		.andDo(print());
	}
	
	
}
