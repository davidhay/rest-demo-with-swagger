package com.ealanta;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
public class UserTasksSwaggerApplication extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(UserTasksSwaggerApplication.class);
	}

	public static void main(String[] args) {
		new UserTasksSwaggerApplication().configure(
				new SpringApplicationBuilder(UserTasksSwaggerApplication.class)).run(args);
	}
	

	@Configuration
	static class Config {

	}
}
