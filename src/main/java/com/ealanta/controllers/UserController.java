package com.ealanta.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import static org.springframework.http.MediaType.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ealanta.domain.Task;
import com.ealanta.domain.User;
import com.ealanta.repository.TaskRepository;
import com.ealanta.repository.UserRepository;

@RestController
@RequestMapping(value="/rest")
public class UserController {

	public static final String[] XML_OR_JSON = {APPLICATION_JSON_VALUE,APPLICATION_XML_VALUE};
	
	@Autowired
	private UserRepository userRepo;

	@Autowired
	private TaskRepository taskRepo;

	@RequestMapping(value = "/users", method=RequestMethod.GET, produces={APPLICATION_JSON_VALUE,APPLICATION_XML_VALUE})
	public Iterable<User> getUsers() {
		return userRepo.findAll();
	}

	@RequestMapping(value = "/users/{id}", method=RequestMethod.GET, produces={APPLICATION_JSON_VALUE,APPLICATION_XML_VALUE})
	public User getUser(@PathVariable("id") Long id) {
		return userRepo.findOne(id);
	}

	@RequestMapping(value = "/users/{userId}/tasks", method=RequestMethod.GET, produces={APPLICATION_JSON_VALUE,APPLICATION_XML_VALUE})
	public List<Task> getUserTasks(@PathVariable("userId") Long userId) {
		return taskRepo.findByUserId(userId);
	}

	@RequestMapping(value = "/users/{userId}/tasks/{taskId}", method=RequestMethod.GET, produces={APPLICATION_JSON_VALUE,APPLICATION_XML_VALUE})
	public ResponseEntity<?> getUserTasks(
			@PathVariable("userId") Long userId,
			@PathVariable("taskId") Long taskId) {
		List<Task> tasks = getUserTasks(userId);
		Optional<Task> task = tasks.stream().filter(t -> t.getId() == taskId)
				.findFirst();
		if (task.isPresent()) {
			return new ResponseEntity<Task>(task.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<String>("cannot find task " + taskId,
					HttpStatus.NOT_FOUND);
		}
	}

}
